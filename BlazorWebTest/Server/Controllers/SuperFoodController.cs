﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BlazorWebTest.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorWebTest.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuperFoodController : ControllerBase
    {
        List<SuperFood> foods = new List<SuperFood>
        {
            new SuperFood { Id = 1, FoodName = "Broccoli", FoodCategory = "Vegetable", ScientificName = "Brassica oleracea"},
            new SuperFood { Id = 2, FoodName = "Kale", FoodCategory = "Vegetable", ScientificName = "Brassica oleracea"},
        };

        [HttpGet]
        public async Task<IActionResult> GetSuperFoods()
        {
            return Ok(foods);
        }

        [HttpGet("{id}")]

        public async Task<IActionResult> GetSingleSuperFood(int id)
        {
            var food = foods.FirstOrDefault(f => f.Id == id);
            if (food == null)
                return NotFound("Super Food was not found.");
            return Ok(food);
        }
    }
}
