﻿using BlazorWebTest.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorWebTest.Client.Services
{
    public interface ISuperFoodService
    {
        Task<List<SuperFood>> GetSuperFoods();
    }
}
