﻿using BlazorWebTest.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace BlazorWebTest.Client.Services
{
    public class SuperFoodService : ISuperFoodService
    {
        private readonly HttpClient _httpClient;

        public SuperFoodService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<List<SuperFood>> GetSuperFoods()
        {
            return await _httpClient.GetFromJsonAsync<List<SuperFood>>("api/superfood");
        }
    }
}
