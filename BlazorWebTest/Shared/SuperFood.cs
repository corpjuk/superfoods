﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlazorWebTest.Shared
{
    public class SuperFood
    {
        public int Id { get; set; }
        public String FoodName { get; set; }
        public String ScientificName { get; set; }

        public String  FoodCategory { get; set; }

    }
}
